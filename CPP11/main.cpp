#include <iostream>
#include <string>
#include <map>

int main()
{
    struct B
    {
        int x;
        int y;
        B(int a, int b) : x(a), y(b) {}
    } b = {123, 321};

    std::cout << b.x << " " << b.y <<std::endl;

    int x[] = {1,2,3};
    float y[4][3] =
    {
        {1, 2, 3},
        {2, 4, 6},
        {3, 5, 7}
    };

    char cv[4] = {'a', 's', 'd', 'f'};
    std::string sa[3] = {"123", "321", "312"};


    struct Foo
    {
        int x;
        double y;
        int z;
        Foo(int, int) {}
    };

    //Foo foo{1, 2.5, 1};

     struct ST
     {
         int x;
         double y;
     protected:
         int z;
     };

     //ST s{1, 2.5, 1};

     struct ST1
     {
         int x;
         double y;
         virtual void F() {}
     };
     //ST1 s {1, 2.5};

     struct ST2
     {
         int x;
         double y = 0.0;
     };
     //ST2 s {1, 2.5};

     struct ST3
     {
         int x;
         double y;
         virtual void F() {}
     private:
         int z;
     public:
         ST3(int i, double j, int k) : x(i), y(j), z(k) {}
     };

     ST3 s {1, 2.5,1};


     //
     int arr[] {1,2,3};

     std::map<std::string,int> mm =
     {
         {"1", 1},
         {"2", 2},
         {"3", 3}
     };

     

    return 0;
}
